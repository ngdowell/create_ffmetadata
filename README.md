# create_ffmetadata

Merge multiple mp3 files (chapters) into a monolithic mp3 file that is tagged with chapter information using the FFMETADATA format.

This is a bash script that will take a number of mp3 files and merge them into a single mp3 file that is tagged with chapter metadata to allow navigation using a suitable player.

Ideal for audiobooks.

**REQUIRED SOFTWARE**

- ffmpeg - this will tag the final mp3 file,
- mediainfo - this probes the input mp3s to work out the chapter timings